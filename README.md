This is an application where you can:
- add things to do; 
- mark them as finished;
- unmark them;
- delete finished todos;
- delete all todos.

I tested it with the NGRok, that's why asgi package is presented 
in requirements.

Screenshot:
![ALT](/Screenshots/Screenshot 2020-04-27 at 11.16.39.png)